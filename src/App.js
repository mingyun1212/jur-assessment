import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Avatar, notification } from "antd"
import { UserOutlined } from '@ant-design/icons';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import Board from './pages/board';
import ConversationsList from "./pages/conversations-list";
import Messages from "./pages/messages";
import './App.scss';


function App() {
  const { app } = useSelector(state=> state)
  const [socket, setSocket] = useState()
  
  useEffect(()=>{
    const avatar = <Avatar className="user-avatar" size={48} icon={<UserOutlined />} style={{marginRight: 30}}/>
    const initWebsocket = () => {
      const endPoint = process.env.REACT_APP_WEB_SOCKET_ENDPOINT
      const ws = new W3CWebSocket(endPoint);
      setSocket(ws)
      //Connect to NotificationsChannel
      ws.onopen = function() {
        ws.send(JSON.stringify({command: 'subscribe',identifier: JSON.stringify({channel: 'NotificationsChannel'})}))
      }
  
      //Websocket response handler
      ws.onmessage = (response) => {
        const data = JSON.parse(response.data)
        const message = data.message
        
        //Only show notification for valid notification body
        if (message && message.id && message.contact_ids.indexOf(app.me.id) > -1 && app.me.id !== message.sender_id) {
          notification.open({
            message: message.sender_name,
            description: message.content,
            icon: avatar
          });
        }
      };
    }
    if (app.me) {
      initWebsocket()
    }
  }, [app.me])


  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/conversations">
            <ConversationsList />
          </Route>
          <Route path="/messages">
            <Messages socket={socket}/>
          </Route>
          <Route path="/">
            <Board />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
