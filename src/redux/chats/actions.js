import { ACTION_TYPE } from '../../utils/constant';

export const createNewConversation = (payload) => {
  return (
    {
      type: ACTION_TYPE.CREATE_CONVERSATION,
      payload
    }
  );
};

export const setCurrentConversation = payload => {
  return (
    {
      type: ACTION_TYPE.SET_CURRENT_CONVERSATION,
      payload
    }
  );
};

export const getMyConversations = () => {
  return (
    {
      type: ACTION_TYPE.GET_MY_CONVERSATIONS
    }
  );
};

export const setMyConversations = payload => {
  return (
    {
      type: ACTION_TYPE.SET_MY_CONVERSATIONS,
      payload
    }
  );
};

export const getMessages = payload => {
  return (
    {
      type: ACTION_TYPE.GET_MESSAGES,
      payload
    }
  );
};


export const setMessages = payload => {
  return (
    {
      type: ACTION_TYPE.SET_MESSAGES,
      payload
    }
  );
};


export const getConversationInfo = payload => {
  return (
    {
      type: ACTION_TYPE.GET_CONVERSATION_INFO,
      payload
    }
  );
};


export const sendMessage = payload => {
  return (
    {
      type: ACTION_TYPE.SEND_MESSAGE,
      payload
    }
  );
};

export const addMessage = payload => {
  return (
    {
      type: ACTION_TYPE.ADD_MESSAGE,
      payload
    }
  );
};