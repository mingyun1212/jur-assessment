import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { ACTION_TYPE } from '../../utils/constant';
import { selectChatUsers } from '../users/actions';
import { axiosInstance } from './../../services/api';
import {
    setCurrentConversation,
    setMyConversations,
    setMessages,
    addMessage
} from './actions';

const getConversationsAsync = () => axiosInstance.get(`/conversations`)

function* getConversations () {
  try {
    const { data } = yield call(getConversationsAsync);
    yield put(setMyConversations(data));
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchGetMyConversation() {
  yield takeEvery(ACTION_TYPE.GET_MY_CONVERSATIONS, getConversations)
}

const createNewConversationAsync = (data) => axiosInstance.post(`/conversations`, data)

function* createNewConversation (action) {
  try {
    const { data } = yield call(createNewConversationAsync, action.payload);
    yield put(setCurrentConversation({...data, isNew: true}));
    yield put(selectChatUsers(data.participants))
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchCreateNewConversation() {
  yield takeEvery(ACTION_TYPE.CREATE_CONVERSATION, createNewConversation)
}

const getMessagesAsync = (id) => axiosInstance.get(`/conversations/${id}/messages`)

function* getMessages (action) {
  try {
    if (!action.payload) return
    const { data } = yield call( getMessagesAsync , action.payload);
    yield put(setMessages(data));
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchGetMessages() {
  yield takeEvery(ACTION_TYPE.GET_MESSAGES, getMessages)
}

const sendMessageAsync = (data) => axiosInstance.post(`/conversations/${data.id}/messages`, {content: data.content})

function* sendMessage (action) {
  try {
    if (!action.payload) return
    const { data } = yield call( sendMessageAsync , action.payload);
    yield put(addMessage(data));
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchSendMessages() {
  yield takeEvery(ACTION_TYPE.SEND_MESSAGE, sendMessage)
}

const getConversationInfoAsync = (id) => axiosInstance.get(`/conversations/${id}`)

function* getConversationInfo (action) {
  try {
    if (!action.payload) return
    const { data } = yield call( getConversationInfoAsync , action.payload);
    yield put(setCurrentConversation(data));
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchGetConversationInfo() {
  yield takeEvery(ACTION_TYPE.GET_CONVERSATION_INFO, getConversationInfo)
}
  
export default function* rootSaga() {
  yield all([
    fork(watchCreateNewConversation),
    fork(watchGetMyConversation),
    fork(watchGetMessages),
    fork(watchSendMessages),
    fork(watchGetConversationInfo),
  ]);
}
  