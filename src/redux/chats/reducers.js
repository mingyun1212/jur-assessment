import { ACTION_TYPE } from "../../utils/constant";
  
  const initialState = {
    currentConversation: null,
    conversationsList: [],
    messages: []
  };  
  
  function chats (state = initialState, action) {
    switch (action.type) {
      case ACTION_TYPE.SET_CURRENT_CONVERSATION:
        if (action.payload.isNew === true) {
          return { ...state, currentConversation: action.payload, messages: [] };
        } else {
          return { ...state, currentConversation: action.payload };
        }
      case ACTION_TYPE.SET_MY_CONVERSATIONS:
        return { ...state, conversationsList: action.payload };
      case ACTION_TYPE.SET_MESSAGES:
        return { ...state, messages: action.payload };
      case ACTION_TYPE.ADD_MESSAGE:
        return { ...state, messages: [...state.messages, action.payload] };
      default:
        return { ...state };
    }
  };
  
  export default chats