import { ACTION_TYPE } from "../../utils/constant";
  
  const initialState = {
    usersList: [],
    participants: []
  };  
  
  function users (state = initialState, action) {
    switch (action.type) {
      case ACTION_TYPE.GET_CONTACTS_SUCCESS:
        return { ...state, usersList: action.payload };
      case ACTION_TYPE.SET_PARTICIPANTS_LIST:
        return { ...state, participants: action.payload };
      case ACTION_TYPE.SET_CHAT_USERS:
        let curChatUsers = state.participants
        const findIndex = curChatUsers.findIndex(el=>el.id === action.payload.id)
        if (findIndex === -1) {
          curChatUsers.push(action.payload)
        } else {
          curChatUsers.splice(findIndex, 1)
        }
        return {...state, participants: curChatUsers}
      default:
        return { ...state };
    }
  };
  
  export default users