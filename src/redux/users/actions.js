import { ACTION_TYPE } from '../../utils/constant';
    
export const getAllUsers = () => {
  return (
    {
      type: ACTION_TYPE.GET_CONTACT,
    }
  );
};
  
export const getAllUsersSuccess = payload => {
  return (
    {
      type: ACTION_TYPE.GET_CONTACTS_SUCCESS,
      payload,
    }
  );
};

export const selectChatUsers = (payload) => {
  return (
    {
      type: ACTION_TYPE.SET_CHAT_USERS,
      payload
    }
  );
};

export const setParticipantsList = (payload) => {
  return (
    {
      type: ACTION_TYPE.SET_PARTICIPANTS_LIST,
      payload
    }
  );
};

