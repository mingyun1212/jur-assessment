import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { ACTION_TYPE } from '../../utils/constant';
import { axiosInstance } from './../../services/api';
import {
  getAllUsersSuccess,
} from './actions';

const getAllUserAsync = () => axiosInstance.get(`/contacts`);

function* getAllUser () {
  try {
    const { data } = yield call(getAllUserAsync);
    yield put(getAllUsersSuccess(data));
  } catch (error) {
    // put exception handler, but leave it for now because I don't know error type
  }
}

export function* watchGetAllUser() {
  yield takeEvery(ACTION_TYPE.GET_CONTACT, getAllUser)
}
  
export default function* rootSaga() {
  yield all([
    fork(watchGetAllUser),
  ]);
}
  