import { ACTION_TYPE, PAGE_TYPE, TITLES } from "../../utils/constant";

  const initialState = {
    title: TITLES.TITLE_WHO_YOU_ARE,
    subTitle: TITLES.TITLE_EMPTY,
    currentPage: PAGE_TYPE.PAGE_MY_ACCOUNT,
    me: null,    
  };
  
  function app (state = initialState, action) {
    switch (action.type) {  
      case ACTION_TYPE.SET_PAGE_INFO:
        return { ...state, ...action.payload };
      case ACTION_TYPE.SET_MY_ACCOUNT:
        return { ...state, me: action.payload };
      default:
        return { ...state };
    }
  };
  
  export default app