import { ACTION_TYPE } from '../../utils/constant';
    
export const setPageInfo = (payload) => {
  return (
    {
      type: ACTION_TYPE.SET_PAGE_INFO,
      payload
    }
  );
};

export const selectMyAccount = (payload) => {
  return (
    {
      type: ACTION_TYPE.SET_MY_ACCOUNT,
      payload
    }
  );
};



