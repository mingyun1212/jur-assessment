import { all } from 'redux-saga/effects';
import usersSagas from './users/saga';
import chatSagas from './chats/saga';


export default function* rootSaga() {
  yield all([
    usersSagas(),
    chatSagas()    
  ]);
}
