import { combineReducers } from 'redux';
import app from './app/reducers';
import users from './users/reducers';
import chats from './chats/reducers';

const reducers = combineReducers({
  app,
  users,
  chats
});


export default reducers;
