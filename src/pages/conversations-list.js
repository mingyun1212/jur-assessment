import { Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { PAGE_TYPE, TITLES } from "../utils/constant";
import { useEffect } from "react";
import ConversationItem from "../components/conversation-item";
import { setPageInfo } from "../redux/app/actions";
import { getMyConversations } from "../redux/chats/actions";
import { useHistory } from "react-router";

function ConversationsList() {
  const { chats, app } = useSelector((state) => state)
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(()=>{
    if (app.me) {
      dispatch(setPageInfo({
        currentPage: PAGE_TYPE.PAGE_CONVERSATION_LIST
      }))
      dispatch(getMyConversations())
    } else {
      history.push('/')
    }
  }, [app.me, history, dispatch])

  const renderHistory = () => {
    let result = []
    chats.conversationsList.forEach(conv => {
      result.push(
        <ConversationItem key={conv.id} conversation={conv} />
      )
    });
    return result;
  }

  const onCreateNewConversation = () => {
    if (!chats.currentConversation) {
      dispatch(setPageInfo({
        title: `${TITLES.TITLE_WELCOME} ${app.me.name}!`,
        subTitle: TITLES.SUB_TITLE_HAVE_NO_CONVERSATIONS,
        currentPage: PAGE_TYPE.PAGE_CHAT_USERS
      }))
      history.push('/')
    } else {
      dispatch(setPageInfo({
        title: chats.currentConversation.title,
        currentPage: PAGE_TYPE.PAGE_MESSAGES
      }))
      history.push('/messages')
    }
  }

  return (
    <div className="conversation-list">
      <div className="title">Your Conversations</div>
      <div>
        {renderHistory()}
      </div>
        
      <div className="footer">
          <div className="action-buttons">
            <Button className="btn-app" onClick={onCreateNewConversation}>{ chats.currentConversation ? 'Open Current Chat' : 'Create New Conversation'}</Button>
          </div>
      </div>
    </div>
  );
}

export default ConversationsList;
