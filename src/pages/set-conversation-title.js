import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input, Row } from "antd";
import UserItem from "../components/user-item";
import { createNewConversation } from "../redux/chats/actions";

function SetConversationTitle() {
  const { users, chats } = useSelector((state) => state)
  const [conversationTitle, setConversationTitle] = useState('')
  const dispatch = useDispatch()
  const history = useHistory()

  useEffect(()=>{
    if (chats.currentConversation && chats.currentConversation.isNew === true) {
      history.push('/messages')
    }
  }, [chats.currentConversation, history])

  const renderChatUsers = () => {
    let result = []
    let row = []
    users.participants.forEach((user, index) => {
      row.push(<UserItem key={user.id} user={user}/>)
      if (index % 2 !== 0) {
        result.push(<Row justify="center" key={`row-${user.id}`}>{row}</Row>)
        row = []
      }
      
    });
    return result;
  }

  const onClickStartConversataion = () => {
    if (conversationTitle) {
      const selectedUserIds = users.participants.map(el=>el.id)
      const param = {
        title: conversationTitle,
        contact_ids: selectedUserIds
      }
      
      dispatch(createNewConversation(param))
    }
  }

  return (
    <>
      <div className="user-list horizontal">
        {renderChatUsers()}
      </div>
      {users.participants.length !== 0 &&
        <div className="footer flex">
          <div className="footer-input">
            <Input 
              className="input-app"
              allowClear
              value={conversationTitle}
              onChange={(e)=>setConversationTitle(e.target.value)}
            />
          </div>
          <div className="action-buttons">
            <Button className="btn-app" onClick={onClickStartConversataion}>Start Conversation</Button>
          </div>
        </div>
      }
    </>
  );
}

export default SetConversationTitle;
