import { Button } from "antd";
import { useDispatch, useSelector } from "react-redux";
import UserItem from "../components/user-item";
import { PAGE_TYPE, TITLES } from "../utils/constant";
import { setPageInfo } from "../redux/app/actions";

function SelectChatUsers() {
  const {app, users} = useSelector((state) => state)
  
  const dispatch = useDispatch()

  const renderChatUsers = () => {
    let result = []
    users.usersList.forEach(user => {
      if (user.id !== app.me.id) {
        result.push(<UserItem key={user.id} user={user}/>)
      }
    });
    return result;
  }

  const onClickContinue = () => {
    dispatch(setPageInfo({
      title: `${TITLES.TITLE_WELCOME} ${app.me.name}!`,
      subTitle: users.participants.length === 0 
        ? TITLES.SUB_TITLE_HAVE_NO_CONVERSATIONS 
        : TITLES.SUB_TITLE_SET_CONVERSATION_TITLE + users.participants.length + ' participants',
      currentPage: PAGE_TYPE.PAGE_CONVERSATION_TITLE
    }))
  }

  return (
    <>
      <div className="body-title">{TITLES.TITLE_SELECT_CONTACTS}</div>
      <div className="user-list">
        {renderChatUsers()}
      </div>
      {users.participants.length !== 0 &&
        <div className="footer">
          <div className="action-buttons">
            <Button className="btn-app" onClick={onClickContinue}>Continue</Button>
          </div>
        </div>
      }
    </>
  );
}

export default SelectChatUsers;
