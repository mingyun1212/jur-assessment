import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input } from "antd";
import { LeftOutlined } from '@ant-design/icons';
import { PAGE_TYPE, TITLES } from "../utils/constant";
import { setPageInfo } from "../redux/app/actions";
import { getMessages, sendMessage, getConversationInfo, addMessage } from "../redux/chats/actions";
import MessageItem from "../components/message-item";
import { setParticipantsList } from "../redux/actions";

function Messages(props) {
  const { socket } = props
  const { chats, app } = useSelector((state) => state)
  const [messageText, setMessageText] = useState('')
  const dispatch = useDispatch()
  const history = useHistory()
  
  if(socket) {
    socket.onmessage = (response) => {
      const data = JSON.parse(response.data)
      const message = data.message
      //Update messages
      if (message && message.id && message.contact_ids.indexOf(app.me.id) > -1 && app.me.id !== message.sender_id) {
        const data = {
          id: message.id,
          sender_id: message.sender_id,
          sender_name: message.sender_name,
          content: message.content
        }
        dispatch(addMessage(data))
      }
    };
  }

  useEffect(()=>{
    if(!chats.currentConversation) {
      history.push('/')
    } else if (chats.currentConversation.id) {
      dispatch(getConversationInfo(chats.currentConversation.id))
      if (chats.currentConversation.isNew !== true) {
        dispatch(getMessages(chats.currentConversation.id))
      }
    }
  // eslint-disable-next-line
  }, [history, dispatch])

  const onClickSend = () => {
    const param = {
      id: chats.currentConversation.id,
      content: messageText
    }
    dispatch(sendMessage(param))
    setMessageText('')
  }

  const onClickEnter = (e) => {
    if (e.keyCode === 13 && messageText) {
      onClickSend()
    }
  }
  
  const onClickBack = () => {
    const participants = chats.currentConversation.participants ? chats.currentConversation.participants : []
    const others = app.me ? participants.filter(el=>el.id !== app.me.id) : []
    dispatch(setParticipantsList(others))
    dispatch(setPageInfo({
      title: `${TITLES.TITLE_WELCOME} ${app.me.name}!`,
      subTitle: TITLES.SUB_TITLE_SET_CONVERSATION_TITLE + others.length + ' participants',
      currentPage: PAGE_TYPE.PAGE_CONVERSATION_TITLE
    }))
    history.push('/')
  }
  
  const renderMessages = () => {
    let result = []
  
    chats.messages.forEach(message => {
      result.push(<MessageItem key={message.id} message={message}/>)
    });
  
    return result
  }

  return (
    <div className="conversation-list">
      <div className="nav-header">
        <Button className="icon-button" type="link" icon={<LeftOutlined />} onClick={onClickBack}/>
          
        <div className="title">{chats.currentConversation ? chats.currentConversation.title : ''}</div>
      </div>
      <div>
        {renderMessages()}
      </div>
      {app.me &&
        <div className="footer flex">
          <div className="footer-input">
            <Input 
              className="input-app"
              allowClear
              value={messageText}
              onKeyDown={(e)=>onClickEnter(e)}
              onChange={(e)=>setMessageText(e.target.value)}
            />
          </div>
          <div className="action-buttons">
            <Button className="btn-app" onClick={onClickSend}>Send</Button>
          </div>
        </div>
      }
    </div>
  );
}

export default Messages;
