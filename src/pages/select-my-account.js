import { Button } from "antd";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import UserItem from "../components/user-item";

function SelectMyAccount(props) {
  const {users, app} = useSelector((state) => state)
  
  const history = useHistory()
  
  const renderUsers = () => {
    let result = []
    users.usersList.forEach(user => {
      result.push(<UserItem key={user.id} user={user}/>)
    });
    return result;
  }

  const onClickContinue = () => {
    history.push('/conversations')
  }

  return (
    <>
      <div className="user-list">
        {renderUsers()}
      </div>
      {app.me &&
        <div className="footer">
          <div className="action-buttons">
            <Button className="btn-app" onClick={onClickContinue}>Continue</Button>
          </div>
        </div>
      }
    </>
  );
}

export default SelectMyAccount;
