import { useEffect } from "react";
import { useDispatch } from "react-redux";
import ChatHeader from "../components/chat-header";
import ChatBody from "../components/chat-body";
import { getAllUsers } from "../redux/users/actions";

function Board() {

  const dispatch = useDispatch()
  
  useEffect(()=>{
    dispatch(getAllUsers())
  }, [dispatch])

  return (
    <div className="main-board">
      <ChatHeader />
      <ChatBody />
    </div>
  );
}

export default Board;
