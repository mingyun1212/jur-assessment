import axios from 'axios';

export const axiosInstance = axios.create({
  baseURL: '/',
  withCredentials: true,
  crossDomain: true,
  headers: {
    'Content-Type': 'application/json',
  },
});

axiosInstance.interceptors.request.use(
  config => {
    const userId = localStorage.getItem("userId")
    config.headers.user_id = userId && userId !== 'null' && !isNaN(parseInt(userId)) ? userId : ''
    return config
  },
  error => Promise.reject(error),
);

axiosInstance.interceptors.response.use(
  response => {
    if (response.data && response.data.error) {
      return Promise.reject(response);
    }
    return response;
  },
  error => {
    if (!error.response) {
      return Promise.reject(error);
    }
    if (error.response.status === 440 || error.response.status === 401) {
    }
    return Promise.reject(error);
  },
);
