import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux'
import App from './App';
import { configureStore } from './redux/store';

test('renders learn react link', () => {
  render(
    <Provider store={configureStore()}>
      <App />
    </Provider>
  );
  // const linkElement = screen.getByText(/learn react/i);
  // expect(linkElement).toBeInTheDocument();
});
