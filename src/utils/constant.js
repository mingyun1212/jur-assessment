export const PAGE_TYPE = {
    PAGE_MY_ACCOUNT: 'pageMyAccount',
    PAGE_CHAT_USERS: 'pageChatUsers',
    PAGE_CONVERSATION_TITLE: 'pageCovnersationTitle',
    PAGE_MESSAGES: 'pageMessages',
    PAGE_CONVERSATION_LIST: 'pageConversationList'
}

export const TITLES = {
    TITLE_EMPTY: '',
    TITLE_WHO_YOU_ARE: 'Let us know who you are',
    TITLE_WELCOME: 'Welcome',
    TITLE_YOUR_CONVERSATIONS: 'Your Conversations',
    TITLE_SELECT_CONTACTS: 'Select contact to message',    
    SUB_TITLE_HAVE_NO_CONVERSATIONS: 'You don\'t have any conversations',
    SUB_TITLE_SET_CONVERSATION_TITLE: 'Give title to start a new conversation with ',
}

export const ACTION_TYPE = {
    GET_CONTACT: 'getContactList',
    GET_CONTACTS_SUCCESS: 'getContactListSuccess',
    GET_CONTACTS_FAILED: 'getContactListFailed',
    SET_PAGE_INFO: 'setPageInfo',
    SET_MY_ACCOUNT: 'setMyAccount',
    SET_CHAT_USERS: 'setChatUsers',
    CREATE_CONVERSATION: 'createConversation',
    SET_CURRENT_CONVERSATION: 'setCurrentConversation',
    GET_MY_CONVERSATIONS: 'getMyConversations',
    SET_MY_CONVERSATIONS: 'setMyConversations',
    GET_MESSAGES: 'getMessages',
    SET_MESSAGES: 'setMessages',
    SEND_MESSAGE: 'sendMessage',
    ADD_MESSAGE: 'addMessage',
    GET_CONVERSATION_INFO: 'getConversationInfo',
    SET_PARTICIPANTS_LIST: 'setParticipantsList'
}
