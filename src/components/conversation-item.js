
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { setCurrentConversation } from '../redux/chats/actions';
import './style.scss'

function ConversationItem(props) {
  const { conversation } = props
  const {app, chats} = useSelector(state=> state)
  const dispatch = useDispatch()

  const onSelectConversation = () => {
    if (chats.currentConversation && chats.currentConversation.id === conversation.id){
      dispatch(setCurrentConversation(null))
    } else {
      dispatch(setCurrentConversation(conversation))
    }
  }

  return (
    <div 
      className={`conversation-item ${chats.currentConversation && chats.currentConversation.id === conversation.id ? 'selected' : ''}`}
      onClick={onSelectConversation}>
      <Avatar className="user-avatar" size={48} icon={<UserOutlined />} />
      <div className="text">
        <p>{conversation.title}</p>
          <h6 className="user-name">
            {conversation.last_message && conversation.last_message.length && conversation.last_message[0].sender_id === app.me.id ? 'You' : conversation.last_message[0] ? conversation.last_message[0].sender_name : ''}
          </h6>
        <p className="last-message">
          { conversation.last_message && conversation.last_message.length ? conversation.last_message[0].content : ''}
        </p>
      </div>
    </div>
  );
}

export default ConversationItem;
