import { useDispatch, useSelector } from "react-redux";
import { Button } from "antd";
import { LeftOutlined } from '@ant-design/icons';
import { TITLES, PAGE_TYPE } from "../utils/constant";
import { setPageInfo } from "../redux/actions";

function ChatHeader() {

  const { app, users } = useSelector((state) => state)
  const dispatch = useDispatch()

  const onClickBack = () => {
    dispatch(setPageInfo({
      subTitle: TITLES.SUB_TITLE_HAVE_NO_CONVERSATIONS,
      currentPage: PAGE_TYPE.PAGE_CHAT_USERS
    }))
  }
  
  return (
    <div className="chat-header">
      { app.subTitle && app.subTitle.indexOf(TITLES.SUB_TITLE_SET_CONVERSATION_TITLE) === 0 && users.participants.length > 0 &&
        <Button className="icon-button" type="link" icon={<LeftOutlined />} onClick={onClickBack}/>
      }
      <div>
        <div className="title">{ app.title }</div>
        <div className="sub-title">{ app.subTitle ? app.subTitle : <span>&nbsp;</span> }</div>
      </div>
    </div>
  );
}

export default ChatHeader;
