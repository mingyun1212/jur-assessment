import { useSelector } from "react-redux";
import SelectChatUsers from "../pages/select-chat-users";
import SelectMyAccount from "../pages/select-my-account";
import SetConversationTitle from '../pages/set-conversation-title'
import ConversationsList from "../pages/conversations-list";
import { PAGE_TYPE } from "../utils/constant";

function ChatBody() {

  const app = useSelector(state=>state.app)

  const renderBody = () => {
    switch(app.currentPage) {
      case PAGE_TYPE.PAGE_MY_ACCOUNT:
        return <SelectMyAccount />
      case PAGE_TYPE.PAGE_CHAT_USERS:
        return <SelectChatUsers />
      case PAGE_TYPE.PAGE_CONVERSATION_TITLE:
        return <SetConversationTitle />
      case PAGE_TYPE.PAGE_CONVERSATION_LIST:
        return <ConversationsList />
      default:
        return <SelectMyAccount />
    }
  }

  return (
    <div className="chat-body">
      {renderBody()}
    </div>
  );
}

export default ChatBody;
