
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { selectMyAccount } from '../redux/app/actions';
import { selectChatUsers } from '../redux/users/actions';
import { PAGE_TYPE } from '../utils/constant';
import './style.scss'

function UserItem(props) {
  const { user } = props
  const {app, users} = useSelector(state=> state)
  const dispatch = useDispatch()

  const onSelectMyAccount = () => {
    switch(app.currentPage) {
      case PAGE_TYPE.PAGE_MY_ACCOUNT:
          if (app.me && app.me.id === user.id) {
            dispatch(selectMyAccount(null))  
          } else {
            dispatch(selectMyAccount(user))  
            localStorage.setItem('userId', user.id)
          }
        break;
      case PAGE_TYPE.PAGE_CHAT_USERS:
        dispatch(selectChatUsers(user))  
        break;
      default:
        break;
    }
  }

  const isExistInSelectChatUsers = () => {
    if (app.currentPage !== PAGE_TYPE.PAGE_MY_ACCOUNT) {
      const findIndex = users.participants.findIndex(el=>el.id === user.id)
      if (findIndex > -1) return true
    }
    return false
  }

  return (
    <div 
      className={`user-item ${app.me && app.me.id === user.id ? 'selected' : ''} ${isExistInSelectChatUsers() ? 'selected' : ''}`}
      onClick={onSelectMyAccount}>
      <Avatar className="user-avatar" size={48} icon={<UserOutlined />} />
      <div className="text">
        <p className="user-name">{user.name}</p>
        <p className="last-message">{'Put here last message'}</p>
      </div>
    </div>
  );
}

export default UserItem;
