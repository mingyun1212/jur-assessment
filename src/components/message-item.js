
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import moment from 'moment';
import './style.scss'

function MessageItem(props) {
  const { message } = props
  const { app } = useSelector(state=> state)
  
  /**
   * Get the text that will be displayed at the bottom of the message item
   * Example format: `You at 12:00PM Today`
   * @returns Formatted text
   */
  const displayPrefix = () => {
    let result = ''
    if (message && app.me) {
      if (message.sender_id === app.me.id) {
        result = 'You'
      } else {
        result = message.sender_name
      }
    } 
    const today = moment();
    const lastSendDay = moment(message.created_at);
    const diff = today.diff(lastSendDay, 'day');
    const time = moment(message.created_at).format('hh:mm A')
    result += ` at ${time} ${diff === 0 ? 'Today' : diff + 'days ago'}`
    return result
  }

  return (
    <div 
      className={`conversation-item ${app.me && app.me.id === message.sender_id ? 'selected' : ''}`}>
      <Avatar className="user-avatar" size={48} icon={<UserOutlined />} />
      <div className="text">
        <p className="last-message">{message.content}</p>
        <h6 className="user-name">{displayPrefix()}</h6>
      </div>
    </div>
  );
}

export default MessageItem;
