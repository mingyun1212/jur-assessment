Please start the app by using below commands

`npm (yarn) install`
`npm (yarn) start`

My implementation:
1. I didn't put the `last message` in `user-item` component that is displayed for user info.
Actually, user object has not last message and I have to call `getMessages` api for each users.
It is not efficient and api response should contain last_message property.
2. Similar issue in notification.
In the websocket payload, it doesn't contain `conversation title`. So I didn't put it on the notification
